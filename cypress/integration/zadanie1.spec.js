const testUrl = 'task_1'
const add = ".input-group-btn"
const firstField = ':nth-child(1) > :nth-child(1) > .thumbnail > .caption > .input-group > .form-control'


describe('Task1', function(){

    beforeEach(function () {
        cy.visit(testUrl)
     })

     it('Check if user can add items to cart by typing amount of product in text field', function(){
        cy.get(firstField)
            .clear()
            .type('1')
            .should('have.value', '1')
        cy.get(add)
            .first()
            .click()
        cy.contains('Łączna ilość produktów: 1')
     })

     it('Check if user can add items to cart by keyboard arrows', function(){
        cy.get(firstField).clear()
            .type('{uparrow}')
            .type('{uparrow}')
            .should('have.value', '2')
        cy.get(add)
            .first()
            .click()
        cy.contains('Łączna ilość produktów: 2')
     })

     it('Check if user is able to delete items from cart', function(){
        cy.get(firstField)
            .clear()
            .type('1')
            .should('have.value', '1')
        cy.get(add)
            .first()
            .click()
        cy.contains('Łączna ilość produktów: 1')
        cy.get('.col-md-3 > .input-group > .input-group-btn > .btn')
        .click()
        cy.contains('Łączna ilość produktów: 0')
     })

     it("Check if user can't add more than 100 items to cart", function(){
        cy.get(firstField)
            .clear()
            .type('101')
            .should('have.value', '101')
        cy.get(add)
            .first()
            .click()
        cy.on('window:alert', (str) => {
            expect(str).to.equal(`Łączna ilość produktów w koszyku nie może przekroczyć 100.`)
        })
    })
})